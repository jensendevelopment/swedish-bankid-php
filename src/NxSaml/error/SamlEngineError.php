<?php
/**
 * Class SamlEngineError | NxSaml/error/SamlEngineError.php
 * 
 * @copyright (c) 2017, Technology Nexus AB
 */

/**
 * This class lists all initialization and validation errors that can occur.
 *
 * @since 1.0.0
 */
abstract class SamlEngineError
{
    //
    // Library initialization error-codes (using SAML metadata)
    //
    /**
     * Invalid configurationPath argument
     */
    const INVALID_CONFIGURATION_PATH = 'Invalid configurationPath argument';
    /**
     * IDP metadata file not found
     */
    const IDP_METADATA_NOT_FOUND = 'IDP metadata file not found';
    /**
     * IDP metadata has invalid certificate
     */
    const IDP_INVALID_CERTIFICATE = 'IDP metadata has invalid certificate';
    /**
     * IDP metadata missing required certificate
     */
    const IDP_MISSING_CERTIFICATE = 'IDP metadata missing required certificate';
    /**
     * IDP metadata is missing SSO descriptor
     */
    const IDP_METADATA_MISSING_SSO_DESCRIPTOR = 'IDP metadata is missing SSO descriptor';
    /**
     * IDP metadata had multiple SSO descriptors
     */
    const IDP_METADATA_MULTIPLE_SSO_DESCRIPTOR = 'IDP metadata had multiple SSO descriptors';
    /**
     * IDP metadata is missing EntityID
     */
    const IDP_ENTITY_ID_MISSING = 'IDP metadata is missing EntityID';
    /**
     * SP metadata file not found
     */
    const SP_METADATA_NOT_FOUND = 'SP metadata file not found';
    /**
     * SP metadata has an invalid AssertionConsumerService location
     */
    const SP_LOCATION_INVALID = 'SP metadata has an invalid AssertionConsumerService location';
    /**
     * SP missing required protocol for AssertionConsumerService
     */
    const SP_LOCATION_NOT_HTTPS = 'SP missing required protocol for AssertionConsumerService';
    /**
     * SP missing AssertionConsumerService for expected binding
     */
    const SP_MISSING_HTTP_REDIRECT = 'SP missing AssertionConsumerService for expected binding';
    /**
     * SP had multiple AssertionConsumerService for expected binding
     */
    const SP_MULTIPLE_HTTP_REDIRECT = 'SP had multiple AssertionConsumerService for expected binding';
    /**
     * SP metadata is missing EntityID
     */
    const SP_ENTITY_ID_MISSING = 'SP metadata is missing EntityID';
    /**
     * SP metadata is missing SSO descriptor
     */
    const SP_METADATA_MISSING_SSO_DESCRIPTOR = 'SP metadata is missing SSO descriptor';
    /**
     * SP metadata had multiple SSO descriptors
     */
    const SP_METADATA_MULTIPLE_SSO_DESCRIPTOR = 'SP metadata had multiple SSO descriptors';
    /**
     * SP metadata is invalid
     */
    const SP_METADATA_INVALID = 'SP metadata is invalid';
    /**
     * IDP metadata is invalid
     */
    const IDP_METADATA_INVALID = 'IDP metadata is invalid';
    
    //
    // Validate SAML Response error-codes
    //
    /**
     * SAML Response not valid XML
     */
    const RESPONSE_NOT_VALID_XML = 'SAML Response not valid XML';
    /**
     * Destination URL did not match URL request was received on
     */
    const INVALID_DESTINATION_URL = 'Destination URL did not match URL request was received on';
    /**
     * Response did not contain any assertion and no encrypted assertions
     */
    const RESPONSE_MISSING_ASSERTIONS = 'Response did not contain any assertion and no encrypted assertions';
    /**
     * Signature verification failed
     */
    const SIGNATURE_VERIFICATION_FAILED = 'Signature verification failed';
    /**
     * Response did not contain a valid Issuer
     */
    const INVALID_ISSUER = 'Response did not contain a valid Issuer';
    /**
     * Response did not contain a valid Subject
     */
    const INVALID_SUBJECT = 'Response did not contain a valid Subject';
    /**
     * Response Subject did not contain a contain NameID value
     */
    const INVALID_SUBJECT_NAMEID_VALUE = 'Response Subject did not contain a contain NameID value';
    /**
     * Response did not contain a valid AuthnStatement
     */
    const INVALID_AUTHN_STATEMENT = 'Response did not contain a valid AuthnStatement';
    /**
     * Assertion did not contain expected Service Provider as audience
     */
    const INVALID_AUDIENCE = 'Assertion did not contain expected Service Provider as audience';
    /**
     * Assertion subject is expired
     */
    const ASSERTION_SUBJECT_EXPIRED = 'Assertion subject is expired';
    /**
     * Assertion subject not yet valid
     */
    const ASSERTION_SUBJECT_NOT_YET_VALID = 'Assertion subject not yet valid';
    /**
     * Assertion is expired
     */
    const ASSERTION_EXPIRED = 'Assertion is expired';
    /**
     * Assertion not yet valid
     */
    const ASSERTION_NOT_YET_VALID = 'Assertion not yet valid';
    /**
     * Assertion did not contain Conditions
     */
    const ASSERTION_MISSING_CONDITIONS = 'Assertion did not contain Conditions';
    /**
     * Assertion did not contain Status
     */
    const ASSERTION_MISSING_STATUS = 'Assertion did not contain Status';
    /**
     * Status did not contain StatusCode
     */
    const STATUS_MISSING_STATUS_CODE = 'Status did not contain StatusCode';
    /**
     * StatusCode did not contain Value
     */
    const STATUS_CODE_MISSING_VALUE = 'StatusCode did not contain Value';
    /**
     * Assertion status was not success
     */
    const STATUS_NOT_SUCCESS = 'Assertion status was not success';
    /**
     * InResponseTo mismatch
     */
    const IN_RESPONSE_TO_MISMATCH = 'InResponseTo mismatch';
    /**
     * Multiple assertions in response
     */
    const MULTIPLE_ASSERTIONS_IN_RESPONSE = 'Multiple assertions in response';
    
    // php specific
    /**
     * Request method not POST
     */
    const RESPONSE_METHOD_NOT_POST = 'Request method not POST';
}
