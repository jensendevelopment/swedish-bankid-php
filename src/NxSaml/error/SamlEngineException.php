<?php
/**
 * Class SamlEngineException | NxSaml/error/SamlEngineException.php
 * 
 * @copyright (c) 2017, Technology Nexus AB
 */

/**
 * This is the specific exception thrown by:
 * LibrarySamlEngine->initialize(...)
 * LibrarySamlEngine->validateSamlResponse()
 *
 */
class SamlEngineException extends Exception
{
    /**
     * Constructor
     * @param SamlEngineError $message An array with 'code' and 'msg' entries
     * @param Exception $previous The original exception
     * @throws InvalidArgumentException If $message is not an array with 'code' and 'msg'
     */
    public function __construct($message, Exception $previous = null)
    {
        if($message) {
            parent::__construct($message, 100, $previous);
        } else {
            throw new InvalidArgumentException("Must use a SamlEngineError type as message");
        }
    }

    /**
     * Generates a human readable string
     * @return string
     */
    public function __toString()
    {
        if (is_null($this->getPrevious())) {
            return "";
        } else {
            return "";
        }
    }
}
