<?php
/**
 * Class LibrarySamlEngine | NxSaml/LibrarySamlEngine.php
 *
 * @copyright (c) 2017, Technology Nexus AB
 */

use RobRichards\XMLSecLibs\XMLSecurityKey;
use NxSaml\Logger;
use NxSaml\Utils;
use \SimpleSAML\Utils\XML;

/**
 * The LibrarySamlEngine is used to validate unsolicited SAML Responses.
 */
class LibrarySamlEngine
{

    /**
     * The IDP metadata.
     *
     * @var array
     */
    private $idpMetadata;

    /**
     * The SP metadata.
     *
     * @var array
     */
    private $spMetadata;

    /**
     * Use this method to get an initialized LibrarySamlEngine instance.
     *
     * @see SamlEngineError
     *
     * @param string $arg0 Either the path to a directory which contains sp.xml and idp.xml or the metadata for the IDP as a string
     * @param string $arg1 Shall be null if $arg0 is the path to a directory otherwise it shall be the metadata of the SP as a string
     * @return \LibrarySamlEngine An instance of the LibrarySamlEngine to be used for validation of SAML requests
     * @throws SamlEngineException on failure
     */
    public static function initialize($arg0, $arg1 = null)
    {
        if (is_null($arg1)) {
            $idpFile = $arg0 . "/idp.xml";
            $spFile = $arg0 . "/sp.xml";

            if (!file_exists($idpFile) || !is_file($idpFile)) {
                throw new SamlEngineException(SamlEngineError::IDP_METADATA_NOT_FOUND);
            }

            if (!file_exists($spFile) || !is_file($spFile)) {
                throw new SamlEngineException(SamlEngineError::SP_METADATA_NOT_FOUND);
            }

            $idp = file_get_contents($idpFile);
            $sp = file_get_contents($spFile);
        } else {
            $idp = $arg0;
            $sp = $arg1;
        }
        return new LibrarySamlEngine($idp, $sp);
    }

    /**
     * The private constructor that validates metadata.
     *
     * @param string $idp
     * @param string $sp
     */
    private function __construct($idp, $sp)
    {
        XML::checkSAMLMessage($idp, 'saml-meta');
        XML::checkSAMLMessage($sp, 'saml-meta');

        Logger::debug('Initializing metadata.');
        try {
            $idpEntities = SimpleSAML_Metadata_SAMLParser::parseDescriptorsString($idp);
        } catch (Exception $e) {
            if (strpos($e->getMessage(), 'entityID') !== false) {
                throw new SamlEngineException(SamlEngineError::IDP_ENTITY_ID_MISSING, $e);
            } else {
                throw new SamlEngineException(SamlEngineError::IDP_METADATA_INVALID, $e);
            }
        }

        if (substr_count($idp, 'IDPSSODescriptor') > 2) {
            throw new SamlEngineException(SamlEngineError::IDP_METADATA_MULTIPLE_SSO_DESCRIPTOR);
        }

        $this->idpMetadata = array_values($idpEntities)[0]->getMetadata20IdP();

        self::validateCertificates(Utils::getPublicKeys($this->idpMetadata, 'signing', false));

        Logger::debug('IDP EntityID: ' . $this->idpMetadata['entityid']);

        try {
            $spEntities = SimpleSAML_Metadata_SAMLParser::parseDescriptorsString($sp);
        } catch (Exception $e) {
            if (strpos($e->getMessage(), 'entityID') !== false) {
                throw new SamlEngineException(SamlEngineError::SP_ENTITY_ID_MISSING, $e);
            } else {
                throw new SamlEngineException(SamlEngineError::SP_METADATA_INVALID, $e);
            }
        }
        $this->spMetadata = array_values($spEntities)[0]->getMetadata20SP();

        self::validateHttpRedirect($this->spMetadata);

        Logger::debug('SP EntityID: ' . $this->spMetadata['entityid']);
    }

    /**
     * Validate HTTP redirect in SP metadata
     *
     * @param array $spMetadata
     * @throws SamlEngineException
     */
    private static function validateHttpRedirect($spMetadata)
    {
        $first = true;
        foreach ($spMetadata['AssertionConsumerService'] as $binding) {
            if ($binding['Binding'] === 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect') {
                if (substr($binding['Location'], 0, 7) === "http://") {
                    throw new SamlEngineException(SamlEngineError::SP_LOCATION_NOT_HTTPS);
                }
                if (!$first) {
                    throw new SamlEngineException(SamlEngineError::SP_MULTIPLE_HTTP_REDIRECT);
                }
                $first = false;
            }
        }
        if ($first) {
            throw new SamlEngineException(SamlEngineError::SP_MISSING_HTTP_REDIRECT);
        }
    }

    /**
     * Validate that signing certificate exists and is valid
     *
     * @param array $certs
     * @throws SamlEngineException
     */
    private static function validateCertificates($certs)
    {
        if (is_null($certs) || count($certs) == 0) {
            throw new SamlEngineException(SamlEngineError::IDP_MISSING_CERTIFICATE);
        }

        $pemKeys = array();
        foreach ($certs as $key) {
            switch ($key['type']) {
                case 'X509Certificate':
                    $pemKeys[] = "-----BEGIN CERTIFICATE-----\n" .
                            chunk_split($key['X509Certificate'], 64) .
                            "-----END CERTIFICATE-----\n";
                    break;
                default:
                    Logger::debug('Skipping unknown key type: ' . $key['type']);
            }
        }

        foreach ($pemKeys as $i => $pem) {
            try {
                $key = new XMLSecurityKey(XMLSecurityKey::RSA_SHA1, array('type' => 'public'));
                $key->loadKey($pem);
            } catch (Exception $e) {
                throw new SamlEngineException(SamlEngineError::IDP_INVALID_CERTIFICATE, $e);
            }
        }
    }

    /**
     * Use this method to validate an unsolicited SAML response.
     *
     * @see SamlEngineError
     *
     * @return \Result on success
     * @throws SamlEngineException on failure
     */
    public function validateSamlResponse()
    {
        return self::processResponse($this->idpMetadata, $this->spMetadata);
    }

    /**
     * Validates response settings
     *
     * @param array $idpMetadata
     * @param array $spMetadata
     *
     * @return \Result on success
     * @throws SamlEngineException on failure.
     * @see SamlEngineError for information about possible errors
     */
    private static function processResponse($idpMetadata, $spMetadata)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            Logger::debug('Request method: POST');
        } else {
            throw new SamlEngineException(
            SamlEngineError::RESPONSE_METHOD_NOT_POST);
        }

        if (empty($_POST['SAMLResponse'])) {
            throw new SamlEngineException(
            SamlEngineError::RESPONSE_NOT_VALID_XML);
        }

        $xmlString = base64_decode($_POST['SAMLResponse']);
        if (!self::isValidXml($xmlString)) {
            throw new SamlEngineException(SamlEngineError::RESPONSE_NOT_VALID_XML);
        }

        $binding = new SAML2_HTTPPost();

        Logger::debug('Validating response signature, if exists');
        try {
            $response = $binding->receive();
        } catch (Exception $e) {
            if (strpos($e->getMessage(), "Missing status code") === 0) {
                throw new SamlEngineException(SamlEngineError::ASSERTION_MISSING_STATUS, $e);
            }
            throw new SamlEngineException(SamlEngineError::RESPONSE_NOT_VALID_XML, $e);
        }

        if (is_null($response->getStatus()) || empty($response->getStatus()) || !key_exists('Code', $response->getStatus()) || empty($response->getStatus()['Code'])) {
            throw new SamlEngineException(
            SamlEngineError::STATUS_CODE_MISSING_VALUE);
        }
        if (!$response->isSuccess()) {
            throw new SamlEngineException(
            SamlEngineError::STATUS_NOT_SUCCESS);
        }

        $idpEntityId = $response->getIssuer();
        if ($idpEntityId != $idpMetadata['entityid']) {
            throw new SamlEngineException(
            SamlEngineError::INVALID_ISSUER);
        }
        Logger::debug('Issuer: ok, ' . $idpEntityId);

        $assertions = $response->getAssertions();
        if (sizeof($assertions) < 1) {
            throw new SamlEngineException(
            SamlEngineError::RESPONSE_MISSING_ASSERTIONS);
        } elseif (sizeof($assertions) > 1) {
            throw new SamlEngineException(
            SamlEngineError::MULTIPLE_ASSERTIONS_IN_RESPONSE);
        }

        $assertion = $assertions[0];

        $subjectConfirmations = $assertion->getSubjectConfirmation();
        if (is_null($subjectConfirmations) || count($subjectConfirmations) === 0 || is_null($subjectConfirmationData = $subjectConfirmations[0]->SubjectConfirmationData)) {
            throw new SamlEngineException(SamlEngineError::INVALID_SUBJECT);
        }
        $subjectConfirmationData = $subjectConfirmations[0]->SubjectConfirmationData;
        if ($subjectConfirmationData->NotBefore !== null && $subjectConfirmationData->NotBefore > time() + 60) {
            throw new SamlEngineException(SamlEngineError::ASSERTION_SUBJECT_NOT_YET_VALID);
        }

        if ($subjectConfirmationData->NotOnOrAfter !== null && $subjectConfirmationData->NotOnOrAfter <= time() - 60) {
            throw new SamlEngineException(SamlEngineError::ASSERTION_SUBJECT_EXPIRED);
        }

        $inResponseTo = $subjectConfirmationData->InResponseTo;
        Logger::debug('InReponseTo: ' . $inResponseTo);

        $expectedDestination = array_values($spMetadata['AssertionConsumerService'])[0]['Location'];
        if (!is_null($response->getDestination()) && $expectedDestination !== $response->getDestination()) {
            Logger::debug($response->getDestination() . ' did not match ' . $expectedDestination);
            throw new SamlEngineException(
            SamlEngineError::INVALID_DESTINATION_URL);
        }
        Logger::debug('Destination: ok, ' . $response->getDestination());

        try {
            $responseSigned = Utils::checkSign($idpMetadata, $response);
        } catch (Exception $e) {
            throw new SamlEngineException(
            SamlEngineError::SIGNATURE_VERIFICATION_FAILED, $e);
        }

        if ($responseSigned) {
            Logger::debug('Response signature: valid');
        } else {
            Logger::debug('Response signature: missing');
        }

        return self::processAssertion(
                        $idpMetadata, $spMetadata, $assertion, $responseSigned, $inResponseTo, $response->getRelayState(), $xmlString);
    }

    /**
     * Check if a string contains valid XML
     *
     * @param string $xml
     * @return boolean true if valid XML otherwise false
     */
    private static function isValidXml($xml)
    {
        try {
            libxml_use_internal_errors(true);
            $doc = new DOMDocument('1.0', 'utf-8');
            $doc->loadXML($xml);
            $errors = libxml_get_errors();
            return empty($errors);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Validates assertion settings
     *
     * @param array $idpMetadata
     * @param array $spMetadata
     * @param type $assertion
     * @param type $responseSigned
     * @param type $relayState
     *
     * @return \Result on success
     * @throws SamlEngineException on failure
     */
    private static function processAssertion(
    $idpMetadata, $spMetadata, $assertion, $responseSigned, $inResponseTo, $relayState, $xmlString)
    {
        if ($assertion->getIssuer() !== $idpMetadata['entityid']) {
            throw new SamlEngineException(
            SamlEngineError::INVALID_ISSUER);
        }

        // Verify assertion signed or response signed
        $assertionSigned = self::checkAssertionSign($idpMetadata, $assertion);
        if (!$assertionSigned && !$responseSigned) {
            throw new SamlEngineException(SamlEngineError::SIGNATURE_VERIFICATION_FAILED);
        }
        Logger::debug('Assertion/Response signature: valid');

        if (is_null($assertion->getNameId())) {
            throw new SamlEngineException(SamlEngineError::INVALID_SUBJECT);
        }
        $nameId = array_values($assertion->getNameId())[0];
        Logger::debug('NameID (output): ' . $nameId);
        Logger::debug('Assertion ID (output): ' . $assertion->getId());
        Logger::debug('AuthnContext (output): ' . $assertion->getAuthnContext());

        $notBefore = $assertion->getNotBefore();
        if ($notBefore !== null && $notBefore > time() + 60) {
            throw new SamlEngineException(
            SamlEngineError::ASSERTION_NOT_YET_VALID);
        }
        Logger::debug('NotBefore: ok, UTC unix time ' . $notBefore);

        $expireTime = PHP_INT_MAX;
        $notOnOrAfter = $assertion->getNotOnOrAfter();
        if ($notOnOrAfter !== null) {
            if ($notOnOrAfter <= time() - 60) {
                throw new SamlEngineException(
                SamlEngineError::ASSERTION_EXPIRED);
            }
            $expireTime = $notOnOrAfter;
        }
        Logger::debug('NotOnOrAfter: ok, UTC unix time ' . $notOnOrAfter);

        $sessionNotOnOrAfter = $assertion->getSessionNotOnOrAfter();
        if ($sessionNotOnOrAfter !== null && $sessionNotOnOrAfter <= time() - 60) {
            throw new SamlEngineException(
            SamlEngineError::ASSERTION_EXPIRED);
        }
        Logger::debug('SessionNotOnOrAfter: ok');

        $validAudiences = $assertion->getValidAudiences();
        if ($validAudiences === null || count($validAudiences) === 0) {
            throw new SamlEngineException(SamlEngineError::ASSERTION_MISSING_CONDITIONS);
        } else {
            $spEntityId = $spMetadata['entityid'];
            if (!in_array($spEntityId, $validAudiences, true)) {
                $candidates = '[' . implode('], [', $validAudiences) . ']';
                Logger::debug('This SP [' . $spEntityId . ']  is not a valid audience for the assertion. Candidates were: ' . $candidates);
                throw new SamlEngineException(SamlEngineError::INVALID_AUDIENCE);
            }
        }
        Logger::debug('Audience: ok, ' . array_values($validAudiences)[0]);

        if (is_null($assertion->getAuthnInstant())) {
            throw new SamlEngineException(SamlEngineError::INVALID_AUTHN_STATEMENT);
        }

        $attributes =  self::getAttributes($xmlString);

        return new Result(
                $nameId, $assertion->getAuthnContext(), $assertion->getId(), $expireTime, $inResponseTo, $relayState, $attributes);
    }

    /**
     * Read the attribute from the XML string to be able to use FriendlyName if it exists
     *
     * @param type $xmlString
     * @return array
     */
    private static function getAttributes($xmlString) {
        $attributes = array();
        $xml = new SimpleXMLElement($xmlString);
        $attributeXml = $xml->xpath("/*[local-name()='Response']/*[local-name()='Assertion']/*[local-name()='AttributeStatement']/*[local-name()='Attribute']");
        if ($attributeXml !== FALSE ) {
            //foreach($attributeXml as $node) {
            while (list( , $node) = each($attributeXml)) {
                $values = array();
                $attributeValues = $node->xpath("./*[local-name()='AttributeValue']");
                while (list( , $value) = each($attributeValues)) {
                    $values[] = "{$value}";
                }
                $key = $node["FriendlyName"] === null ? $node["Name"] : $node["FriendlyName"];
                if (array_key_exists("{$key}", $attributes)) {
                    $attributes["{$key}"] = array_merge($attributes["{$key}"], $values);
                } else {
                    $attributes["{$key}"] = $values;
                }
            }
        }
        return $attributes;
    }

    /**
     * Method to validate signature in assertion.
     *
     * @param array $idpMetadata
     * @param SAML2\Assertion $assertion
     * @return type boolean
     * @throws SamlEngineException if signature verification fails.
     */
    private static function checkAssertionSign($idpMetadata, $assertion)
    {
        try {
            return Utils::checkSign($idpMetadata, $assertion);
        } catch (Exception $e) {
            throw new SamlEngineException(
            SamlEngineError::SIGNATURE_VERIFICATION_FAILED, $e);
        }
    }
}
