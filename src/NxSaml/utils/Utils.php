<?php

namespace NxSaml;

use RobRichards\XMLSecLibs\XMLSecurityKey;
use NxSaml\Logger;

class Utils
{

    /**
     * Check the signature on a SAML2 message or assertion.
     *
     * @param SimpleSAML_Configuration $srcMetadata  The metadata of the sender.
     * @param SAML2_SignedElement $element  Either a SAML2_Response or a SAML2_Assertion.
     */
    public static function checkSign($srcMetadata, $element)
    {

        /* Find the public key that should verify signatures by this entity. */
        $keys = self::getPublicKeys($srcMetadata, 'signing');
        if ($keys !== null) {
            $pemKeys = array();
            foreach ($keys as $key) {
                switch ($key['type']) {
                case 'X509Certificate':
                    $pemKeys[] = "-----BEGIN CERTIFICATE-----\n" .
                        chunk_split($key['X509Certificate'], 64) .
                        "-----END CERTIFICATE-----\n";
                    break;
                default:
                    Logger::debug('Skipping unknown key type: ' . $key['type']);
                }
            }
        } else {
            throw new Exception(
                    'Missing certificate in metadata for ' .
                    var_export($srcMetadata->getString('entityid'), true));
        }

        Logger::debug('Has ' . count($pemKeys) . ' candidate keys for validation.');

        $lastException = null;
        foreach ($pemKeys as $i => $pem) {
            $key = new XMLSecurityKey(XMLSecurityKey::RSA_SHA1, array('type'=>'public'));
            $key->loadKey($pem);

            try {
                /*
                 * Make sure that we have a valid signature on either the response
                 * or the assertion.
                 */
                $res = $element->validate($key);
                if ($res) {
                    Logger::debug('Validation with key #' . $i . ' succeeded.');
                    return true;
                }
                Logger::debug('Validation with key #' . $i . ' failed without exception.');
            } catch (Exception $e) {
                Logger::debug('Validation with key #' . $i . ' failed with exception: ' . $e->getMessage());
                $lastException = $e;
            }
        }

        /* We were unable to validate the signature with any of our keys. */
        if ($lastException !== null) {
            throw $lastException;
        } else {
            return false;
        }
    }


    public static function getPublicKeys($srcMetadata, $use = null, $required = true, $prefix = '')
    {
        assert('is_bool($required)');
        assert('is_string($prefix)');

        if (self::hasValue($prefix.'keys', $srcMetadata)) {
            $ret = array();
            foreach (self::getArray($prefix.'keys', $srcMetadata) as $key) {
                if ($use !== null && isset($key[$use]) && !$key[$use]) {
                    continue;
                }
                if (isset($key['X509Certificate'])) {
                    // Strip whitespace from key
                    $key['X509Certificate'] = preg_replace('/\s+/', '', $key['X509Certificate']);
                }
                $ret[] = $key;
            }
            if (!empty($ret)) {
                return $ret;
            }
        }
        if ($required) {
            throw new Exception($this->location.': Missing certificate in metadata.');
        } else {
            return null;
        }
    }

    public static function hasValue($name, $arr)
    {
        return array_key_exists($name, $arr);
    }

    public static function getArray($name, $arr)
    {
        assert('is_string($name)');

        $ret = self::getValue($name, $arr);

        if (!is_array($ret)) {
            throw new Exception('The option '.var_export($name, true).' is not an array.');
        }

        return $ret;
    }

    public static function getValue($name, $arr)
    {
        // return the default value if the option is unset
        if (!array_key_exists($name, $arr)) {
            throw new Exception(
                'Could not retrieve the required option '.
                var_export($name, true)
            );
        }

        return $arr[$name];
    }
}
