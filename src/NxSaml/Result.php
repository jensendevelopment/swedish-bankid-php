<?php
/**
 * Class Result | NxSaml/Result.php
 *
 * @copyright (c) 2017, Technology Nexus AB
 */

/**
 * The Result class is the output of a successful
 * call to LibrarySamlEngine.validateSamlResponse()
 */
class Result
{

    /**
     * The name ID
     *
     * @var string|null
     */
    private $nameId = null;

    /**
     * A URI reference identifying an authentication context class that describes the authentication context
     * @var string
     */
    private $authnContextClassRef;

    /**
     * The assertion ID
     *
     * @var string
     */
    private $assertionId;

    /**
     * The relay state
     *
     * @var string|null
     */
    private $relayState;

    /**
     * The in response to ID
     *
     * @var string|null
     */
    private $inResponseTo;

    /**
     * The expire time
     *
     * @var int
     */
    private $expireTime;

    /**
     * The attributes
     *
     * @var array
     */
    private $attributes;

    /**
     * Do not use this constructor directly.
     * The LibrarySamlEngine will create a Result on validation success.
     *
     * @param string $nameId
     * @param string $authnContextClassRef
     * @param string $assertionId
     * @param int $expireTime
     * @param string $inResponseTo
     * @param string $relayState
     * @param array $attributes
     */
    public function __construct(
            $nameId, $authnContextClassRef, $assertionId, $expireTime, $inResponseTo = null, $relayState = null, $attributes = null)
    {
        $this->nameId  = $nameId;
        $this->authnContextClassRef = $authnContextClassRef;
        $this->assertionId = $assertionId;
        $this->relayState = $relayState;
        $this->expireTime = $expireTime;
        $this->inResponseTo = $inResponseTo;
        $this->attributes = $attributes;
    }

    /**
     * Use this method to get the NameID from the validated SAML response.
     *
     * This can be a username, or a national identification number (NIN), depending on the method used when
     * authenticating.
     *
     * @return string The specific NameID
     */
    public function getNameId()
    {
        return $this->nameId;
    }

    /**
     * Use this method to get a URI that specifies the method the user used when authenticating.
     * @return string The specific authnContextClassRef
     */
    public function getAuthnContextClassRef()
    {
        return $this->authnContextClassRef;
    }

    /**
     * Use this method to get the assertion ID from the validated SAML response
     *
     * @return string The specific assertion ID
     */
    public function getAssertionId()
    {
        return $this->assertionId;
    }

    /**
     * Use this method to get the relay state
     *
     * @return string|null The specific relay state
     */
    public function getRelayState()
    {
        return $this->relayState;
    }

    /**
     * Use this method to get the inResponseTo id
     *
     * @return string|null The specific inResponseTo
     */
    public function getInResponseTo()
    {
        return $this->inResponseTo;
    }

    /**
     * Use this method to get the expire time from the validated SAML response
     *
     * @return int The specific expire time
     */
    public function getExpireTime()
    {
        return $this->expireTime;
    }

    /**
     * Use this method to get the attributes from the validated SAML response
     *
     * @return array The attributes from the SAML response
     */
    public function getAttributes()
    {
        return $this->attributes;
    }
}
