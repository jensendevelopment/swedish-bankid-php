<?php

require __DIR__.'/../vendor/autoload.php';
require_once('NxSaml/utils/Logger.php');
require_once('NxSaml/utils/Utils.php');
require_once('NxSaml/error/SamlEngineError.php');
require_once('NxSaml/error/SamlEngineException.php');
require_once('NxSaml/Result.php');
require_once('NxSaml/LibrarySamlEngine.php');
