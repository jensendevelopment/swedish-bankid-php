<?php
// simplesamlphp depends on saml2, 
// *BUT* saml2 also depends on simplesamlphp, seems circular :-(
require_once('SimpleSAML/lib/SimpleSAML/Utilities.php');
require_once('SimpleSAML/lib/SimpleSAML/Configuration.php');
require_once('SimpleSAML/lib/SimpleSAML/Utils/Config.php');
require_once('SimpleSAML/lib/SimpleSAML/XML/XML.php');
require_once('SimpleSAML/lib/SimpleSAML/XML/Errors.php');
require_once('SimpleSAML/lib/SimpleSAML/Utils/Random.php');
require_once('SimpleSAML/lib/SimpleSAML/Metadata/SAMLParser.php');